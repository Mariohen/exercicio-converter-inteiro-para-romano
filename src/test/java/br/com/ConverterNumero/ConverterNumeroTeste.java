package br.com.ConverterNumero;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ConverterNumeroTeste {

    @Test
    public void testarNumeroInteiroMenorQueZero(){
        ConverterNumero converterNumero = new ConverterNumero(-1);
        Assertions.assertThrows(RuntimeException.class, ()-> {converterNumero.validarNumeroInteiro();});
    }

    @Test
    public void testarNumeroInteiroIgualaZero(){
        ConverterNumero converterNumero = new ConverterNumero(0);
        Assertions.assertThrows(RuntimeException.class, ()-> {converterNumero.validarNumeroInteiro();});
    }

    @Test
    public void testarNumeroInteiroIgualaUm(){
        ConverterNumero converterNumero = new ConverterNumero(1);
        String resultado = converterNumero.retornarNumeroRomano();
        Assertions.assertEquals("I", resultado);
    }

    @Test
    public void testarNumeroInteiroIguala1500(){
        ConverterNumero converterNumero = new ConverterNumero(1500);
        String resultado = converterNumero.retornarNumeroRomano();
        Assertions.assertEquals("MD", resultado);
    }

}
