package br.com.ConverterNumero;

import java.util.Scanner;

public class ConverterNumero {

    int numeroInteiro;

    public ConverterNumero(int numeroInteiro) {
        this.numeroInteiro = numeroInteiro;
    }

    public void validarNumeroInteiro(){
        if(this.numeroInteiro <= 0){
            throw new RuntimeException("Numero deve ser maior que 0");
        }
    }

    public String retornarNumeroRomano () {

        String contatenaNumero = "";
        int[] numeroInteiro = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
        String[] numeroRomano = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};

        int contator = 0;
        while (this.numeroInteiro > 0) {
            if (this.numeroInteiro  >= numeroInteiro[contator]) {
               contatenaNumero = contatenaNumero + numeroRomano[contator];
                this.numeroInteiro = this.numeroInteiro - numeroInteiro[contator];
            } else {
                contator++;
            }
         System.out.println(numeroRomano);
        } return contatenaNumero;
    }
}
